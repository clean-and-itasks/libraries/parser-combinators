definition module Text.Parsers.Simple.Core

from Control.Applicative import class pure, class <*>, class Applicative, class *>, class <*, class Alternative
from Control.Monad import class Monad, class MonadPlus
from Data.Either import :: Either
from Data.Functor import class Functor
from StdOverloaded import class ==
from StdClass import class Eq

:: Parser t a =: Parser ([t] -> ([(a, [t])], [String]))

// AMF instances
instance Functor (Parser t)
instance pure (Parser t)
instance <*> (Parser t)
instance *> (Parser t)
instance <* (Parser t)
instance Alternative (Parser t)
instance Monad (Parser t)
instance MonadPlus (Parser t)

// Functions to run the parser
parse     :: !(Parser t a) [t] -> Either [String] a
runParser :: !(Parser t a) [t] -> ([(a, [t])], [String])

// Core combinators
pFail    :: Parser t a
pYield   :: a -> Parser t a
pSatisfy :: (t -> Bool) -> Parser t t
pError   :: String -> Parser t a
pPeek :: Parser t [t]

// Convenience parsers
(@!) infixr 4 :: (Parser t a) String -> Parser t a

(<<|>) infixr 4 :: (Parser t a) (Parser t a) -> Parser t a
(<|>>) infixr 4 :: (Parser t a) (Parser t a) -> Parser t a

(<:>) infixr 6 :: !(Parser s r) (Parser s [r]) -> Parser s [r]

/**
 * Applies the given parser 0 or more times, until it fails.
 *
 * @param The parser.
 * @result Parser containing the list of results from applying the parser given as a parameter.
 */
pMany     :: (Parser s r) -> Parser s [r]

/**
 * Applies the given parser 1 or more times, until it fails. If it could not be applied once, the parser fails.
 *
 * @param The parser.
 * @result Parser containing the list of results from applying the parser given as a parameter.
 */
pSome     :: !(Parser s r) -> Parser s [r]

/**
 * Parses some tokens (1 or more) until a token for which the predicate holds is found, this token is not parsed.
 * This parser fails if not at least one token could be parsed until the predicate holds for a token.
 *
 * @param The predicate.
 * @result Parser with the list of tokens for which the predicate did not hold until a token matching the predicate
 *         was found.
 */
pSomeTill :: !(t -> Bool) -> Parser t [t]

/**
 * Parses many tokens (0 or more) until a token for which the predicate holds is found, this token is not parsed.
 *
 * @param The predicate.
 * @result Parser with the list of tokens for which the predicate did not hold until a token matching the predicate
 *         was found.
 */
pManyTill :: !(t -> Bool) -> Parser t [t]

pOptional :: !(Parser s r) (r -> Parser s r) -> Parser s r
pOneOf    :: [t] -> Parser t t | Eq t
pChainl   :: !(Parser t a) (Parser t (a a -> a)) a -> Parser t a
pChainl1  :: !(Parser t a) (Parser t (a a -> a)) -> Parser t a
pToken    :: t -> Parser t t | == t
pSepBy    :: !(Parser t a) (Parser t s) -> Parser t [a]
pSepBy1   :: !(Parser t a) (Parser t s) -> Parser t [a]
pList     :: ![Parser t a] -> Parser t [a]
