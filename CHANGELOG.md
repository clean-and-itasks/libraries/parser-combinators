# Changelog

#### 1.1.1

- Chore: support base `3.0`.

### 1.1.0

- Feature: `Text.Parsers.Simple.Core`: add `someTill` and `manyTill`.
- Feature: `Text.Parsers.Simple.Chars`: add `pSingleQuote` and `pDoubleQuote`.

## 1.0.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  parser combinator modules.
